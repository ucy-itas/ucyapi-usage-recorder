<?php

return [
    'redis' => [
        'key' => 'usage-records',
        'timeout' => 2,
        'enabled' => false,
        'db' => 0
    ],
    'db' => [
        'enabled' => false
    ],
    'log' => [
        'enabled' => false
    ]
];