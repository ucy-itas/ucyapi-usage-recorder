<?php
namespace Iss\Api\Service\UsageRecorder\Writer;

use Iss\Api\Service\UsageRecorder\WriterInterface;
use Phalcon\{Config\Config, Logger\Logger};

class Log implements WriterInterface
{
    protected Logger $_client;

    protected Config $_config;

    public function __construct(Config $config)
    {
        $this->_config = $config;
        $this->_client = $config->logger;
    }

    public function write(array $stats, $close = false)
    {
        $this->_client->info("[usage_recorder] Statistics: ".json_encode($stats)." [%uuid%]");
    }

    public function getClient()
    {
        return $this->_client;
    }
}