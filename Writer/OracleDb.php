<?php
namespace Iss\Api\Service\UsageRecorder\Writer;

use Iss\Api\Backend\Db\Pdo;
use Iss\Api\Service\UsageRecorder\WriterInterface;
use PDOStatement;
use Phalcon\Config\Config;

class OracleDb implements WriterInterface
{
    protected ?Pdo $_client;

    protected ?PDOStatement $_statement;

    protected Config $_config;

    public function __construct(Config $config)
    {
        $this->_config = $config;
        $this->_client = null;
        $this->_statement = null;
    }

    public function write(array $stats, $close = false)
    {
        if (!$this->_client) {
            $this->_client = new Pdo($this->_config);
        }

        if (!$this->_statement) {
            $query = "begin stats.add(".
                "p_duration => ?, p_client => ?, p_actual_client => ?, ".
                "p_route => ?, p_method => ?, p_status_code => ?, ".
                "p_user => ?, p_memory => ?, p_on => ?, ".
                "p_setup_duration => ?, p_uuid => ?); end;";
            $this->_statement = $this->_client->prepare($query);
        }
        $statement = $this->_statement;
        $date = date('Y-m-d H:i:s', $stats['on']);
        //$statement->bindParam(1, $res, \PDO::PARAM_STR, 10);
        $statement->bindParam(1, $stats['time'], \PDO::PARAM_STR, 25);
        $statement->bindParam(2, $stats['client']);
        $statement->bindParam(3, $stats['actual_client']);
        $statement->bindParam(4, $stats['route']);
        $statement->bindParam(5, $stats['method']);
        $statement->bindParam(6, $stats['status_code']);
        $statement->bindParam(7, $stats['user']);
        $statement->bindParam(8, $stats['memory']);
        $statement->bindParam(9, $date);
        $statement->bindParam(10, $stats['setup_time'], \PDO::PARAM_STR, 25);
        $statement->bindParam(11, $stats['uuid']);

        $result = $statement->execute();
        if ($close) {
            $this->_client = null;
            $this->_statement = null;
        }
        if (!$result) {
            var_dump($this->_client->errorCode());
            var_dump($this->_client->errorInfo());
            return false;
        }
        return $result;
    }

}