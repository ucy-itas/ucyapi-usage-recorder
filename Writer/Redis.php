<?php
namespace Iss\Api\Service\UsageRecorder\Writer;

use Iss\Api\Service\UsageRecorder\WriterInterface;
use Phalcon\Config\Config;

class Redis implements WriterInterface
{
    protected \Redis $_client;

    protected Config $_config;

    protected bool $_connected;

    public function __construct(Config $config)
    {
        $this->_config = $config;
        $this->_client = new \Redis();
        $this->_connected = false;
    }

    public function write(array $stats, $close = false)
    {
        if ($this->connect()) {
            $this->_client->sAdd($this->_config->key, json_encode($stats));
            if ($close) {
                $this->_client->close();
                $this->_connected = false;
            }
        }
    }

    public function connect()
    {
        if (!$this->_connected) {
            $timeout = $this->_config->timeout ?? 60;
            $this->_connected = $this->_client->connect($this->_config->host, $this->_config->port, $timeout);
        }
        return $this->_connected;
    }

    public function getClient()
    {
        return $this->_client;
    }
}