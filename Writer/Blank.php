<?php
namespace Iss\Api\Service\UsageRecorder\Writer;

use Iss\Api\Service\UsageRecorder\WriterInterface;
use Phalcon\Config\Config;

class Blank implements WriterInterface
{
    public function __construct(Config $config)
    {
        return;
    }

    public function write(array $stats, $close = false)
    {
        return;
    }

    public function getClient()
    {
        return null;
    }
}