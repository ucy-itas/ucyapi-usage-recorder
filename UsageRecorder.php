<?php
namespace Iss\Api\Service\UsageRecorder;

use Iss\Api\ServiceInterface;
use Iss\Api\Service\UsageRecorder\Writer\{Blank, OracleDb, Redis, Log};
use Phalcon\{Config\Config, Mvc\Micro, Events\Event};
use Iss\Api\ServiceTrait;

class UsageRecorder implements ServiceInterface
{
    use ServiceTrait;

    protected array $stats;

    protected ?Config $config = null;

    /**
     * @var WriterInterface[] $writers
     */
    protected array $writers = [];

    public function __construct()
    {
        $this->stats = ['time' => 0];
        $this->setPriority(50);
    }

    public function register(Micro $application) : bool
    {
        if (!$application->getDI()->has('config')) {
            // config service is not ready yet
            return false;
        }

        if (is_null($this->config)) {
            $this->config = $application['config'](
                'service/' . self::getName(), __DIR__ . '/config'
            )->merge(
                $application['config']('service/' . self::getName())
            );
        }

        if ($this->config->path('log.enabled', false) && !$application->getDI()->has('logger')) {
            // logger service is not ready yet
            return false;
        }
        if ($this->config->path('redis.enabled', false)) {
            //unset($this->_config->redis->enabled);
            $this->writers[] = new Redis($this->config->redis);
        }
        if ($this->config->path('db.enabled', false)) {
            unset($this->config->db->enabled);
            $this->writers[] = new OracleDb($this->config->db);
        }
        if ($this->config->path('log.enabled', false)) {
            $this->writers[] = new Log(new Config(['logger' => $application['logger']]));
        }
        if (count($this->writers) === 0) {
            $this->writers[] = new Blank(null);
        }

        $application->eventsManager->attach('micro', $this, $this->getPriority());
        $application->eventsManager->attach('api', $this, $this->getPriority());
        $application->getDI()->setShared($this->getName(), $this);
        return true;
    }

    public function unregister(Micro $application): ?ServiceInterface
    {
        $application->getEventsManager()->detach('micro', $this);
        $application->getEventsManager()->detach('api', $this);
        $application->getDI()->remove($this->getName());
        return $this;
    }

    public static function getName() : string
    {
        return 'usagerecorder';
    }

    public function beforeHandleRoute(Event $event, Micro $application, $return_value = null)
    {
        $request = $application['request'];
        $this->stats['client'] = $request->getClientAddress(true);
        $this->stats['actual_client'] = $request->getClientAddress();
        $this->stats['method'] = $request->getMethod();
        return true;
    }

    public function beforeHandleRequest(Event $event, Micro $application)
    {
        $current_time = microtime(true);
        $this->stats['setup_time'] = $GLOBALS['request_duration'] + $current_time;
        $this->stats['time'] = -$current_time;
        $this->stats['uuid'] = $application->uuid;
        return true;
    }

    public function afterHandleRequest(Event $event, Micro $application)
    {
        if (is_object($application->getRouter()->getMatchedRoute())) {
            $this->stats['route'] = $application->getRouter()->getMatchedRoute()->getName();
        } else {
            $this->stats['route'] = '-';
        }

        $response = $application['response'];
        $logger = $application['logger'];
        $logger['service'] = self::getName();
        $this->stats['status_code'] = $response->getHeaders()->get('Status');

        $principal = $application['authentication']->getPrincipal();
        $this->stats['user'] = $principal['name'] ?? 'not-authenticated';

        $this->stats['time'] += microtime(true);
        $this->stats['memory'] = memory_get_peak_usage();
        $this->stats['on'] = time();

        foreach ($this->writers as $writer) {
            try {
                $writer->write($this->stats, true);
            } catch (\Exception $e) {
                $application['logger']->warning('[%service%] ' . $e->getMessage());
            }
        }

        return true;
    }
}