<?php
namespace Iss\Api\Service\UsageRecorder;

interface WriterInterface
{
    public function write(array $stats, $close = false);
}